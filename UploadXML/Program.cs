﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UploadXML.Properties;
using System.Configuration;
using UploadXML.Classes;
using Prokey_Utils;

namespace UploadXML
{
    class Program
    {
        public static  string _emailuser;
        public static string _password;
        public static string _apiUrl;
        public static int _roleId;
        public static int _clienteId;

        //Envio mail.
        public static string _destinatarioEmail ;
        public static string _destinatarioCopia ;
        public static string _direccionEnvio ;
        public static string _nicEnvio ;
        public static string _asuntoEnvio ;
        public static string _passwordDireccionEnvio ;
        public static string _puertoSmtp ;
        public static string _servidorSmtp ;

        static void Main(string[] args)
        {
            
            _emailuser = ConfigurationManager.AppSettings["emailuser"];
            _password = ConfigurationManager.AppSettings["pass"];
            _apiUrl = ConfigurationManager.AppSettings["apiUrl"];
            _roleId = Convert.ToInt32(ConfigurationManager.AppSettings["roleId"]);
            _clienteId = Convert.ToInt32(ConfigurationManager.AppSettings["clienteId"]);

            //Envio mail.
            _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];

            if (!Directory.Exists(Settings.Default.INPUT_FOLDER)) throw new Exception("Directorio de input (" + Settings.Default.INPUT_FOLDER + ") no encontrado.");
            else if (!Directory.Exists(Settings.Default.OUTPUT_FOLDER)) throw new Exception("Directorio de output (" + Settings.Default.OUTPUT_FOLDER + ") no encontrado.");
            else if (!Directory.Exists(Settings.Default.LOG_FOLDER)) throw new Exception("Directorio de logs (" + Settings.Default.LOG_FOLDER + ") no encontrado.");
            else
            {
                //Recupero el Token de Acceso.
                string _token = API.GetInstance(_emailuser, _password, _apiUrl).Token;
                int CantidadErrores = 0;
                IEnumerable<string> InputFiles = Program.GetInputs();
                try
                {
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("roles_")))
                    {

                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("idiomas_")))
                    {

                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("paises_")))
                    {

                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("empresas_")))
                    {

                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("user_")))
                    {
                        Classes.ttUser client = Classes.ttUser.GetFromFile(xml);
                        string ErrorMessage = string.Empty;
                        if (client.Process(_apiUrl, _token, out ErrorMessage))
                        {
                            File.Delete(xml);
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(), "NO SE PROCESO CORRECTAMENTE el archivo: " + xml + " " + ErrorMessage);
                            if (File.Exists(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty)))
                                File.Delete(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            File.Move(xml, Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            CantidadErrores++;
                        }
                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("clientes_")))
                    {
                        Classes.ttCliente client = Classes.ttCliente.GetFromFile(xml);
                        string ErrorMessage = string.Empty;
                        if(client.Process(_apiUrl, _token, out ErrorMessage))
                        {
                            File.Delete(xml);
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(), "NO SE PROCESO CORRECTAMENTE el archivo: " + xml + " " + ErrorMessage);
                            if (File.Exists(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty)))
                                File.Delete(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            File.Move(xml, Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            CantidadErrores++;
                        }
                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("tipoproyecto_")))
                    {

                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("tipoTarea_")))
                    {
                        Classes.ttTipoTarea client = Classes.ttTipoTarea.GetFromFile(xml);
                        string ErrorMessage = string.Empty;
                        if (client.Process(_apiUrl, _token, out ErrorMessage))
                        {
                            File.Delete(xml);
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(), "NO SE PROCESO CORRECTAMENTE EL ARCHIVO: " + xml + " " + ErrorMessage);
                            if (File.Exists(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty)))
                                File.Delete(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            File.Move(xml, Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            CantidadErrores++;
                        }
                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("proyecto_")))
                    {
                        Classes.ttProyectos project = Classes.ttProyectos.GetFromFile(xml);
                        string ErrorMessage = string.Empty;
                        if (project.Process(_apiUrl, _token, out ErrorMessage))
                        {
                            File.Delete(xml);
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(), "NO SE PROCESO CORRECTAMENTE EL ARCHIVO: " + xml + " " + ErrorMessage);
                            if (File.Exists(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty)))
                                File.Delete(Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            File.Move(xml, Settings.Default.OUTPUT_FOLDER + xml.Replace(Settings.Default.INPUT_FOLDER, string.Empty));
                            CantidadErrores++;
                        }
                    }
                    foreach (string xml in InputFiles.Where(f => f.Replace(Settings.Default.INPUT_FOLDER, string.Empty).StartsWith("tareas_")))
                    {

                    }
                    if (CantidadErrores > 0)
                    {
                        // Enviar email de errores
                        //Mail.Enviar("", "pinsua@prokey.com.ar", "soporte@prokey.com.ar", "soporte@prokey.com.ar", "TimeSheet Subida Tanoira",
                        //    "Problema con la subida de Informacion.", "Se produjo un error en la subida de los xml a la web. Por favor revise el log.",
                        //    true, "soporte2018", 587, "smtp.gmail.com", true);
                        Mail.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio,
                            _asuntoEnvio, "Se produjo un error en la subida de los xml a la web. Por favor revise el log.",
                            true, _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);

                    }

                }
                catch (Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(), "Se produjo el siguiente error: " + ex.Message);

                    //Mail.Enviar("", "pinsua@prokey.com.ar", "soporte@prokey.com.ar", "soporte@prokey.com.ar", "TimeSheet Subida Tanoira",
                    //        "Problema con la subida de Informacion.", "Se produjo un error en la subida de los xml a la web. Por favor revise el log.",
                    //        true, "soporte2018", 587, "smtp.gmail.com", true);
                    Mail.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio,
                           _asuntoEnvio, "Se produjo un error en la subida de los xml a la web. Por favor revise el log.",
                           true, _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                }
            }           
        }

        private static IEnumerable<string> GetInputs()
        {
            return Directory.EnumerateFiles(Settings.Default.INPUT_FOLDER, "*.xml", SearchOption.TopDirectoryOnly);
        }
    }
}
