﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace UploadXML.Classes
{
    [XmlRoot("ttCliente")]
    public class ttCliente
    {
        [XmlElement(typeof(ttClienteRow))]
        public ttClienteRow ttClienteRow;

        public static ttCliente GetFromFile(string File)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ttCliente));
            using (StreamReader reader = new StreamReader(File, Encoding.GetEncoding(1252)))
            {
                return (ttCliente)serializer.Deserialize(reader);
            }
        }

        public bool Process(string _url, string _email, string _pass,
            out string ErrorMessage)
        {
            switch (this.ttClienteRow.actualizar)
            {
                case "A":
                    {
                        if (this.Exists(_url, _email, _pass)) return this.Put(_url, _email, _pass, out ErrorMessage);
                        return this.Post(_url, _email, _pass, out ErrorMessage);
                    }
                case "M": return this.Put(_url, _email, _pass, out ErrorMessage);
                case "B": return this.Delete(_url, _email, _pass, out ErrorMessage);
                default: throw new Exception("Campo actualizar (" + this.ttClienteRow.actualizar + ") incorrecto.");
            }
        }

        /// <summary>
        /// Procesa el xml de Cliente.
        /// </summary>
        /// <param name="_url">URL de la Api.</param>
        /// <param name="_token">Token de Acceso.</param>
        /// <param name="ErrorMessage"></param>
        /// <returns>True/False</returns>
        public bool Process(string _url, string _token,
            out string ErrorMessage)
        {
            switch (this.ttClienteRow.actualizar)
            {
                case "A":
                    {
                        if (this.Exists(_url, _token)) return this.Put(_url, _token, out ErrorMessage);
                        return this.Post(_url, _token, out ErrorMessage);
                    }
                case "M": return this.Put(_url, _token, out ErrorMessage);
                case "B": return this.Delete(_url, _token, out ErrorMessage);
                default: throw new Exception("Campo actualizar (" + this.ttClienteRow.actualizar + ") incorrecto.");
            }
        }

        public static APIClient Get(string _url, string _email, string _pass, string cliente_erp_id, string empresa_id)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("cliente/clientebyerpid/" + cliente_erp_id + "/empresa/" + empresa_id , Method.GET);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            IRestResponse<List<APIClient>> response = client.Execute<List<APIClient>>(request);
            return response.Data.FirstOrDefault();
        }

        /// <summary>
        /// Recupera el cliente.
        /// </summary>
        /// <param name="_url">URL de la API.</param>
        /// <param name="cliente_erp_id">Nro de cliente del ERP.</param>
        /// <param name="empresa_id">Empresa del Cliente.</param>
        /// <param name="_token">Token de Acceso.</param>
        /// <returns>Cliente</returns>
        public static APIClient Get(string _url, string cliente_erp_id, string empresa_id, string _token)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("cliente/clientebyerpid/" + cliente_erp_id + "/empresa/" + empresa_id, Method.GET);
            request.AddParameter("token", _token);
            IRestResponse<List<APIClient>> response = client.Execute<List<APIClient>>(request);
            return response.Data.FirstOrDefault();
        }

        private bool Exists(string _url, string _email, string _pass)
        {
            return (ttCliente.Get(_url, _email, _pass, this.ttClienteRow.cliente_ID.ToString(), this.ttClienteRow.empresa_ID.ToString()) != null);
        }

        private bool Exists(string _url, string _token)
        {
            return (ttCliente.Get(_url, this.ttClienteRow.cliente_ID.ToString(), this.ttClienteRow.empresa_ID.ToString(), _token) != null);
        }

        private bool Post(string _url, string _email, string _pass, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("cliente", Method.POST);
            request.AddParameter("token", API.GetInstance(_email,_pass,_url).Token);
            request.AddParameter("name", this.ttClienteRow.nombre);
            request.AddParameter("empresa_id", this.ttClienteRow.empresa_ID);
            request.AddParameter("cliente_erp_id", this.ttClienteRow.cliente_ID);
            request.AddParameter("habilitado", this.ttClienteRow.habilitado);
            request.AddParameter("cuit", this.ttClienteRow.cuit);
            request.AddParameter("pais_id", this.ttClienteRow.pais_ID);
            request.AddParameter("idioma_id", this.ttClienteRow.idioma_ID);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }
        
        /// <summary>
        /// Post de Cliente
        /// </summary>
        /// <param name="_url">URL de la API</param>
        /// <param name="_token">Token de acceso.</param>
        /// <param name="ErrorMessage">Error.</param>
        /// <returns>True/False.</returns>
        private bool Post(string _url, string _token, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("cliente", Method.POST);
            request.AddParameter("token", _token);
            request.AddParameter("name", this.ttClienteRow.nombre);
            request.AddParameter("empresa_id", this.ttClienteRow.empresa_ID);
            request.AddParameter("cliente_erp_id", this.ttClienteRow.cliente_ID);
            request.AddParameter("habilitado", this.ttClienteRow.habilitado);
            request.AddParameter("cuit", this.ttClienteRow.cuit);
            request.AddParameter("pais_id", this.ttClienteRow.pais_ID);
            request.AddParameter("idioma_id", this.ttClienteRow.idioma_ID);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Put(string _url, string _email, string _pass, out string ErrorMessage)
        {
            var cliente = ttCliente.Get(_url, _email, _pass, this.ttClienteRow.cliente_ID, this.ttClienteRow.empresa_ID.ToString());
            if (cliente != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("cliente/" + cliente.id.ToString(), Method.PUT);
                request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                request.AddParameter("name", this.ttClienteRow.nombre);
                request.AddParameter("empresa_id", this.ttClienteRow.empresa_ID);
                request.AddParameter("cliente_erp_id", this.ttClienteRow.cliente_ID);
                request.AddParameter("habilitado", this.ttClienteRow.habilitado);
                request.AddParameter("cuit", this.ttClienteRow.cuit);
                request.AddParameter("pais_id", this.ttClienteRow.pais_ID);
                request.AddParameter("idioma_id", this.ttClienteRow.idioma_ID);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El cliente " + this.ttClienteRow.cliente_ID + " no exite en la nube.";
                return false;
            }
        }
        /// <summary>
        /// Put de Cliente
        /// </summary>
        /// <param name="_url">URL de la API</param>
        /// <param name="_token">Token de Acceso.</param>
        /// <param name="ErrorMessage">Error.</param>
        /// <returns>True/False.</returns>
        private bool Put(string _url, string _token, out string ErrorMessage)
        {
            var cliente = ttCliente.Get(_url, this.ttClienteRow.cliente_ID, this.ttClienteRow.empresa_ID.ToString(), _token);
            if (cliente != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("cliente/" + cliente.id.ToString(), Method.PUT);
                request.AddParameter("token", _token);
                request.AddParameter("name", this.ttClienteRow.nombre);
                request.AddParameter("empresa_id", this.ttClienteRow.empresa_ID);
                request.AddParameter("cliente_erp_id", this.ttClienteRow.cliente_ID);
                request.AddParameter("habilitado", this.ttClienteRow.habilitado);
                request.AddParameter("cuit", this.ttClienteRow.cuit);
                request.AddParameter("pais_id", this.ttClienteRow.pais_ID);
                request.AddParameter("idioma_id", this.ttClienteRow.idioma_ID);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El cliente " + this.ttClienteRow.cliente_ID + " no exite en la nube.";
                return false;
            }
        }

        private bool Delete(string _url, string _email, string _pass, out string ErrorMessage)
        {
            var cliente = ttCliente.Get(_url, _email, _pass, this.ttClienteRow.cliente_ID, this.ttClienteRow.empresa_ID.ToString());
            if (cliente != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("cliente/" + cliente.id.ToString(), Method.DELETE);
                request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El cliente " + this.ttClienteRow.cliente_ID + " no existe en la nnube.";
                return false;
            }
        }
        /// <summary>
        /// Delete de Clinte
        /// </summary>
        /// <param name="_url">URL de la API.</param>
        /// <param name="_token">Token de Acceso.</param>
        /// <param name="ErrorMessage">Error.</param>
        /// <returns>True/False.</returns>
        private bool Delete(string _url, string _token, out string ErrorMessage)
        {
            var cliente = ttCliente.Get(_url, this.ttClienteRow.cliente_ID, this.ttClienteRow.empresa_ID.ToString(), _token);
            if (cliente != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("cliente/" + cliente.id.ToString(), Method.DELETE);
                request.AddParameter("token", _token);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El cliente " + this.ttClienteRow.cliente_ID + " no existe en la nnube.";
                return false;
            }
        }
    }

    [Serializable()]
    public class ttClienteRow
    {
        [XmlElement("empresa_ID")]
        public int empresa_ID;

        [XmlElement("cliente_ID")]
        public string cliente_ID;

        [XmlElement("idioma_ID")]
        public int idioma_ID;

        [XmlElement("habilitado")]
        public int habilitado;

        [XmlElement("nombre")]
        public string nombre;

        [XmlElement("cuit")]
        public string cuit;

        [XmlElement("pais_ID")]
        public int pais_ID;

        [XmlElement("actualizar")]
        public string actualizar;
    }
}
