﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace UploadXML.Classes
{
    [XmlRoot("ttTipoTarea")]
    public class ttTipoTarea
    {
        [XmlElement(typeof(ttTipoTareaRow))]
        public ttTipoTareaRow ttTipoTareaRow;

        public static ttTipoTarea GetFromFile(string File)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ttTipoTarea));
            using (StreamReader reader = new StreamReader(File, Encoding.GetEncoding(1252)))
            {
                return (ttTipoTarea)serializer.Deserialize(reader);
            }
        }

        public bool Process(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            switch (this.ttTipoTareaRow.actualizar)
            {
                case "A": return this.Post(_url, _email, _pass, out ErrorMessage);
                case "M": return this.Put(_url, _email, _pass, out ErrorMessage);
                case "B": return this.Delete(_url, _email, _pass, out ErrorMessage);
                default: throw new Exception("Campo actualizar (" + this.ttTipoTareaRow.actualizar + ") incorrecto.");
            }
        }

        public bool Process(string _url, string _token, out string ErrorMessage)
        {
            switch (this.ttTipoTareaRow.actualizar)
            {
                //case "A": return this.Post(_url, _token, out ErrorMessage);
                case "A":
                    {
                        if (this.Exists(_url, _token)) return this.Put(_url, _token, out ErrorMessage);
                        return this.Post(_url, _token, out ErrorMessage);
                    }
                case "M": return this.Put(_url, _token, out ErrorMessage);
                case "B": return this.Delete(_url, _token, out ErrorMessage);
                default: throw new Exception("Campo actualizar (" + this.ttTipoTareaRow.actualizar + ") incorrecto.");
            }
        }

        private bool Post(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea", Method.POST);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            request.AddParameter("name", this.ttTipoTareaRow.nombre);
            request.AddParameter("tipo_proyecto_id", this.ttTipoTareaRow.tipoproyecto_ID);
            request.AddParameter("facturable", this.ttTipoTareaRow.facturable);
            request.AddParameter("tiempo_maximo", this.ttTipoTareaRow.tiempo_maximo);
            request.AddParameter("tiempo_minimo", this.ttTipoTareaRow.tiempo_minimo);
            request.AddParameter("fraccion", this.ttTipoTareaRow.fraccion);
            request.AddParameter("tipo_tarea_erp_id", this.ttTipoTareaRow.tipotarea_ERP_ID);
            request.AddParameter("empresa_id", this.ttTipoTareaRow.empresa_ID);
            request.AddParameter("role_id", "2");
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Post(string _url, string _token, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea", Method.POST);
            request.AddParameter("token", _token);
            request.AddParameter("name", this.ttTipoTareaRow.nombre);
            request.AddParameter("tipo_proyecto_id", this.ttTipoTareaRow.tipoproyecto_ID);
            request.AddParameter("facturable", this.ttTipoTareaRow.facturable);
            request.AddParameter("tiempo_maximo", this.ttTipoTareaRow.tiempo_maximo);
            request.AddParameter("tiempo_minimo", this.ttTipoTareaRow.tiempo_minimo);
            request.AddParameter("fraccion", this.ttTipoTareaRow.fraccion);
            request.AddParameter("tipo_tarea_erp_id", this.ttTipoTareaRow.tipotarea_ERP_ID);
            request.AddParameter("empresa_id", this.ttTipoTareaRow.empresa_ID);
            request.AddParameter("role_id", "2");
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Put(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea/" + this.ttTipoTareaRow.tipoproyecto_ID.ToString(), Method.PUT);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            request.AddParameter("name", this.ttTipoTareaRow.nombre);
            request.AddParameter("tipo_proyecto_id", this.ttTipoTareaRow.tipoproyecto_ID);
            request.AddParameter("facturable", this.ttTipoTareaRow.facturable);
            request.AddParameter("tiempo_maximo", this.ttTipoTareaRow.tiempo_maximo);
            request.AddParameter("tiempo_minimo", this.ttTipoTareaRow.tiempo_minimo);
            request.AddParameter("fraccion", this.ttTipoTareaRow.fraccion);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Put(string _url, string _token, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea/" + this.ttTipoTareaRow.tipoproyecto_ID.ToString(), Method.PUT);
            request.AddParameter("token", _token);
            request.AddParameter("name", this.ttTipoTareaRow.nombre);
            request.AddParameter("tipo_proyecto_id", this.ttTipoTareaRow.tipoproyecto_ID);
            request.AddParameter("facturable", this.ttTipoTareaRow.facturable);
            request.AddParameter("tiempo_maximo", this.ttTipoTareaRow.tiempo_maximo);
            request.AddParameter("tiempo_minimo", this.ttTipoTareaRow.tiempo_minimo);
            request.AddParameter("fraccion", this.ttTipoTareaRow.fraccion);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Delete(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea/" + this.ttTipoTareaRow.tipoproyecto_ID.ToString(), Method.DELETE);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Delete(string _url, string _token, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea/" + this.ttTipoTareaRow.tipoproyecto_ID.ToString(), Method.DELETE);
            request.AddParameter("token", _token);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        /// <summary>
        /// Recupera el cliente.
        /// </summary>
        /// <param name="_url">URL de la API.</param>
        /// <param name="cliente_erp_id">Nro de cliente del ERP.</param>
        /// <param name="empresa_id">Empresa del Cliente.</param>
        /// <param name="_token">Token de Acceso.</param>
        /// <returns>Cliente</returns>
        public static APITipoTarea Get(string _url, string tipotarea_erp_id, string empresa_id, string _token)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("tipotarea/tipotareabyerpid/" + tipotarea_erp_id + "/empresa/" + empresa_id, Method.GET);
            request.AddParameter("token", _token);
            IRestResponse<List<APITipoTarea>> response = client.Execute<List<APITipoTarea>>(request);
            return response.Data.FirstOrDefault();
        }

        private bool Exists(string _url, string _token)
        {
            return (ttTipoTarea.Get(_url, this.ttTipoTareaRow.tipotarea_ERP_ID.ToString(), this.ttTipoTareaRow.empresa_ID.ToString(), _token) != null);
        }
    }

    [Serializable()]
    public class ttTipoTareaRow
    {
        [XmlElement("tipoproyecto_ID")]
        public int tipoproyecto_ID;

        [XmlElement("empresa_ID")]
        public int empresa_ID;

        [XmlElement("nombre")]
        public string nombre;

        [XmlElement("tiempo_maximo")]
        public int tiempo_maximo;

        [XmlElement("tiempo_minimo")]
        public int tiempo_minimo;

        [XmlElement("fraccion")]
        public int fraccion;

        [XmlElement("facturable")]
        public int facturable;

        [XmlElement("actualizar")]
        public string actualizar;

        [XmlElement("tipotarea_ERP_ID")]
        public string tipotarea_ERP_ID;

    }
}
