﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UploadXML.Classes
{
    public class APITipoTarea
    {
        public int id { get; set; }
        public string name { get; set; }
        public int tipo_proyecto_id { get; set; }
        public int role_id { get; set; }
        public int facturable { get; set; }
        public decimal tiempo_maximo { get; set; }
        public decimal tiempo_minimo { get; set; }
        public decimal fraccion { get; set; }
        public int empresa_id { get; set; }
        public string tipo_tarea_erp_id { get; set; }

    }
}
