﻿using RestSharp;

namespace UploadXML.Classes
{
    public class API
    {

        private static API Instance;
        public string Token;

        private API(string Token)
        {
            this.Token = Token;
        }

        //public static API GetInstance()
        //{
        //    if (API.Instance == null)
        //    {
        //        var client = new RestClient("http://api-timebilling.prokey.com.ar:8080/api/v1");
        //        var request = new RestRequest("auth/login", Method.POST);
        //        request.AddParameter("email", "admin@prokey-sa.com");
        //        request.AddParameter("password", "Pk.2639");
        //        IRestResponse<Auth> response = client.Execute<Auth>(request);
        //        API.Instance = new API(response.Data.token);
        //    }
        //    return API.Instance;
        //}

        public static API GetInstance(string _email, string _pass, string _api)
        {
            if (API.Instance == null)
            {
                //var client = new RestClient("http://api-timebilling.prokey.com.ar:8080/api/v1");
                var client = new RestClient(_api);
                var request = new RestRequest("auth/login", Method.POST);
                request.AddParameter("email", _email);
                request.AddParameter("password", _pass);
                IRestResponse<Auth> response = client.Execute<Auth>(request);
                API.Instance = new API(response.Data.token);
            }
            return API.Instance;
        }
    }
}
