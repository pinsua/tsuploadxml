﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace UploadXML.Classes
{
    [XmlRoot("ttUser")]
    public class ttUser
    {

        [XmlElement(typeof(ttUserRow))]
        public ttUserRow ttUserRow;

        public static ttUser GetFromFile(string File)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ttUser));
            using (StreamReader reader = new StreamReader(File, Encoding.GetEncoding(1252)))
            {
                return (ttUser)serializer.Deserialize(reader);
            }
        }

        public bool Process(string _url, string _email, string _pass,
            out string ErrorMessage)
        {
            try
            {
                switch (this.ttUserRow.actualizar)
                {
                    case "A":
                        {
                            if (this.Exists(_url, _email, _pass, out ErrorMessage))
                                return this.Put(_url, _email, _pass, out ErrorMessage);
                            return this.Post(_url, _email, _pass, out ErrorMessage);
                        }
                    case "M": return this.Put(_url, _email, _pass, out ErrorMessage);
                    case "B": return this.Delete(_url, _email, _pass, out ErrorMessage);
                    default: throw new Exception("Campo actualizar (" + this.ttUserRow.actualizar + ") incorrecto.");
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
        }
        /// <summary>
        /// Procesa un usuario.
        /// </summary>
        /// <param name="_url">URL`de la Api.</param>
        /// <param name="_token">Token de acceso.</param>
        /// <param name="ErrorMessage">Error.</param>
        /// <returns>True/False</returns>
        public bool Process(string _url, string _token, out string ErrorMessage)
        {
            int NubeId = 0;
            try
            {
                switch (this.ttUserRow.actualizar)
                {
                    case "A":
                        {
                            if (this.Exists(_url, _token, out ErrorMessage, out NubeId))
                                return this.Put(_url, _token, NubeId, out ErrorMessage);
                            return this.Post(_url, _token, out ErrorMessage);
                        }
                    case "M": return this.Put(_url, _token, 0, out ErrorMessage);
                    case "B": return this.Delete(_url, _token, out ErrorMessage);
                    default: throw new Exception("Campo actualizar (" + this.ttUserRow.actualizar + ") incorrecto.");
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
        }

        private bool Exists(string _url, string _email, string _pass, out string ErrorMessage)
        {
            try
            {
                var client = new RestClient(_url);
                //var request = new RestRequest("user", Method.GET);
                // api/v1/empresa/{id_mysql}/usuarios
                var request = new RestRequest("empresa/" + ttUserRow.empresa_ID + "/usuarios", Method.GET);
                request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                IRestResponse<List<APIUser>> response = client.Execute<List<APIUser>>(request);
                ErrorMessage = string.Empty;
                //vemos que tenemos.
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + response.StatusCode.ToString();
                    return false;
                }
                else
                {
                    return response.Data.Exists(u => u.usuario_erp_id == this.ttUserRow.user_ERP_id && u.empresa_id == this.ttUserRow.empresa_ID);
                }
            }
            catch(Exception ex)
            {
                ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + ex.Message;
                return false;
            }

        }

        private bool Exists(string _url, string _token, out string ErrorMessage, out int NubeId)
        {
            try
            {
                var client = new RestClient(_url);
                
                //var request = new RestRequest("user", Method.GET);
                // api/v1/empresa/{id_mysql}/usuarios
                var request = new RestRequest("empresa/" + ttUserRow.empresa_ID + "/usuarios", Method.GET);
                request.AddParameter("token", _token);
                IRestResponse<List<APIUser>> response = client.Execute<List<APIUser>>(request);
                ErrorMessage = string.Empty;
                NubeId = 0;
                //vemos que tenemos.
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + response.StatusCode.ToString();
                    return false;
                }
                else
                {
                    if (response.Data.Exists(u => u.usuario_erp_id == this.ttUserRow.user_ERP_id && u.empresa_id == this.ttUserRow.empresa_ID))
                    {
                        NubeId = response.Data.Find(u => u.usuario_erp_id == this.ttUserRow.user_ERP_id && u.empresa_id == this.ttUserRow.empresa_ID).id;
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + ex.Message;
                NubeId = 0;
                return false;
            }

        }

        private bool NewExists(string _url, string _token, out string ErrorMessage)
        {
            try
            {
                var client = new RestClient(_url);
                //api/v1/user/userbyerpid/{id_user_erp}/empresa/{id_empresa_mysql}
                var request = new RestRequest("user/userbyerpid/" + ttUserRow.user_ERP_id + "/empresa/" + ttUserRow.empresa_ID, Method.GET);
                request.AddParameter("token", _token);
                IRestResponse<List<APIUser>> response = client.Execute<List<APIUser>>(request);
                //IRestResponse<APIUser> response = client.Execute<APIUser>(request);
                ErrorMessage = string.Empty;
                //vemos que tenemos.
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + response.StatusCode.ToString();
                    return false;
                }
                else
                {
                    if (response.Data.Count > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + ex.Message;
                return false;
            }

        }

        private bool Post(string _url, string _email, string _pass,
            out string ErrorMessage)
        {
            try
            {
                var client = new RestClient(_url);
                var request = new RestRequest("user", Method.POST);
                request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                request.AddParameter("name", this.ttUserRow.userName);
                request.AddParameter("email", this.ttUserRow.userEmail);
                request.AddParameter("password", this.ttUserRow.user_passw);
                request.AddParameter("habilitado", this.ttUserRow.habilitado);
                request.AddParameter("usuario_erp_id", this.ttUserRow.user_ERP_id);
                request.AddParameter("empresa_id", this.ttUserRow.empresa_ID);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    //ErrorMessage = "NO VA";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + ex.Message;
                return false;
            }
        }

        private bool Post(string _url, string _token, out string ErrorMessage)
        {
            try
            {
                var client = new RestClient(_url);
                var request = new RestRequest("user", Method.POST);
                request.AddParameter("token", _token);
                request.AddParameter("name", this.ttUserRow.userName);
                request.AddParameter("email", this.ttUserRow.userEmail);
                request.AddParameter("password", this.ttUserRow.user_passw);
                request.AddParameter("habilitado", this.ttUserRow.habilitado);
                request.AddParameter("usuario_erp_id", this.ttUserRow.user_ERP_id);
                request.AddParameter("empresa_id", this.ttUserRow.empresa_ID);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    //ErrorMessage = "NO VA";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "se produjo el siguiente error al consultar si existe un usuario. Error: " + ex.Message;
                return false;
            }
        }

        private bool Put(string _url, string _email, string _pass,
            out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("user/" + this.ttUserRow.user_ERP_id, Method.PUT);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            request.AddParameter("name", this.ttUserRow.userName);
            request.AddParameter("email", this.ttUserRow.userEmail);
            request.AddParameter("password", this.ttUserRow.user_passw);
            request.AddParameter("habilitado", this.ttUserRow.habilitado);
            request.AddParameter("usuario_erp_id", this.ttUserRow.user_ERP_id);
            request.AddParameter("empresa_id", this.ttUserRow.empresa_ID);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Put(string _url, string _token, int _nubeId, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            //var request = new RestRequest("user/" + this.ttUserRow.user_ERP_id, Method.PUT);
            var request = new RestRequest("user/" + _nubeId.ToString(), Method.PUT);
            request.AddParameter("token", _token);
            request.AddParameter("name", this.ttUserRow.userName);
            request.AddParameter("email", this.ttUserRow.userEmail);
            request.AddParameter("password", this.ttUserRow.user_passw);
            request.AddParameter("habilitado", this.ttUserRow.habilitado);
            request.AddParameter("usuario_erp_id", this.ttUserRow.user_ERP_id);
            request.AddParameter("empresa_id", this.ttUserRow.empresa_ID);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Delete(string _url, string _email, string _pass,
            out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("user/" + this.ttUserRow.user_ERP_id, Method.DELETE);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

        private bool Delete(string _url, string _token, out string ErrorMessage)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("user/" + this.ttUserRow.user_ERP_id, Method.DELETE);
            request.AddParameter("token", _token);
            IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                ErrorMessage = string.Empty;
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                ErrorMessage = "Internal Server Error.";
                return false;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ErrorMessage = response.Content;
                return false;
            }
            else
            {
                ErrorMessage = response.Data.rta;
                return false;
            }
        }

    }

    [Serializable()]
    public class ttUserRow
    {
        [XmlElement("userName")]
        public string userName;

        [XmlElement("userEmail")]
        public string userEmail;

        [XmlElement("user_passw")]
        public string user_passw;

        [XmlElement("habilitado")]
        public int habilitado;

        [XmlElement("user_ERP_id")]
        public string user_ERP_id;

        [XmlElement("empresa_ID")]
        public int empresa_ID;

        [XmlElement("actualizar")]
        public string actualizar;
    }
}
