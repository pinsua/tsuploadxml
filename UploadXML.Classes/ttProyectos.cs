﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace UploadXML.Classes
{
    [XmlRoot("ttProyectos")]
    public class ttProyectos
    {
        [XmlElement(typeof(ttProyectosRow))]
        public ttProyectosRow ttProyectosRow;

        public static ttProyectos GetFromFile(string File)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ttProyectos));
            using (StreamReader reader = new StreamReader(File, Encoding.GetEncoding(1252)))
            {
                return (ttProyectos)serializer.Deserialize(reader);
            }
        }

        public bool Process(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            switch (this.ttProyectosRow.actualizar)
            {
                case "A":
                    {
                        if (this.Exists(_url, _email, _pass)) return this.Put(_url, _email, _pass, out ErrorMessage);
                        return this.Post(_url, _email, _pass, out ErrorMessage);
                    }
                case "M": return this.Put(_url, _email, _pass, out ErrorMessage);
                case "B": return this.Delete(_url, _email, _pass, out ErrorMessage);
                default: throw new Exception("Campo actualizar (" + this.ttProyectosRow.actualizar + ") incorrecto.");
            }
        }

        public bool Process(string _url, string _token, out string ErrorMessage)
        {
            switch (this.ttProyectosRow.actualizar)
            {
                case "A":
                    {
                        if (this.Exists(_url, _token)) return this.Put(_url, _token, out ErrorMessage);
                        return this.Post(_url, _token, out ErrorMessage);
                    }
                case "M": return this.Put(_url, _token, out ErrorMessage);
                case "B": return this.Delete(_url, _token, out ErrorMessage);
                default: throw new Exception("Campo actualizar (" + this.ttProyectosRow.actualizar + ") incorrecto.");
            }
        }

        public static APIProject Get(string _url, string _email, string _pass, int proyecto_erp_id)
        {
            var client = new RestClient("http://api-timebilling.prokey.com.ar:8080/api/v1");
            var request = new RestRequest("proyecto/proyectoByErpId/" + proyecto_erp_id.ToString(), Method.GET);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            IRestResponse<List<APIProject>> response = client.Execute<List<APIProject>>(request);
            return response.Data.FirstOrDefault();
        }

        public static APIProject Get(string _url, string _email, string _pass, int proyecto_erp_id, int empresa_id)
        {
            var client = new RestClient("http://api-timebilling.prokey.com.ar:8080/api/v1");
            var request = new RestRequest("proyecto/proyectobyerpid/" + proyecto_erp_id.ToString() + "/empresa/" + empresa_id.ToString(), Method.GET);
            request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
            IRestResponse<List<APIProject>> response = client.Execute<List<APIProject>>(request);
            return response.Data.FirstOrDefault();
        }

        public static APIProject Get(string _url, string _token, int proyecto_erp_id, int empresa_id)
        {
            var client = new RestClient(_url);
            var request = new RestRequest("proyecto/proyectobyerpid/" + proyecto_erp_id.ToString() + "/empresa/" + empresa_id.ToString(), Method.GET);
            request.AddParameter("token", _token);
            IRestResponse<List<APIProject>> response = client.Execute<List<APIProject>>(request);
            return response.Data.FirstOrDefault();
        }

        private bool Exists(string _url, string _email, string _pass)
        {
            //return (ttProyectos.Get(_url, _email, _pass, this.ttProyectosRow.proyecto_ID) != null);
            return (ttProyectos.Get(_url, _email, _pass, this.ttProyectosRow.proyecto_ID, this.ttProyectosRow.empresa_ID) != null);
        }

        private bool Exists(string _url, string _token)
        {
            return (ttProyectos.Get(_url, _token, this.ttProyectosRow.proyecto_ID, this.ttProyectosRow.empresa_ID) != null);
        }

        private bool Post(string _url, string _email, string _pass,
            out string ErrorMessage)
        {
            var cliente = ttCliente.Get(_url, _email, _pass, this.ttProyectosRow.cliente_ID, this.ttProyectosRow.empresa_ID.ToString());
            if (cliente != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("proyecto", Method.POST);
                request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                request.AddParameter("name", this.ttProyectosRow.descripcion);
                request.AddParameter("empresa_id", this.ttProyectosRow.empresa_ID);
                request.AddParameter("tipo_proyecto_id", this.ttProyectosRow.tipoproyecto_ID);
                request.AddParameter("proyecto_erp_id", this.ttProyectosRow.proyecto_ID);
                request.AddParameter("proyecto_erp_id2", this.ttProyectosRow.proyecto_ID2);
                request.AddParameter("sector", this.ttProyectosRow.sector);
                request.AddParameter("idioma_id", this.ttProyectosRow.idioma_ID);
                request.AddParameter("facturable", this.ttProyectosRow.facturable);
                request.AddParameter("habilitado", this.ttProyectosRow.habilitado);
                request.AddParameter("cliente_id", cliente.id);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El Cliente " + this.ttProyectosRow.cliente_ID.ToString() + " no existe en la nube.";
                return false;
            }
        }

        private bool Post(string _url, string _token, out string ErrorMessage)
        {
            var cliente = ttCliente.Get(_url, this.ttProyectosRow.cliente_ID, this.ttProyectosRow.empresa_ID.ToString(), _token);
            if (cliente != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("proyecto", Method.POST);
                request.AddParameter("token", _token);
                request.AddParameter("name", this.ttProyectosRow.descripcion);
                request.AddParameter("empresa_id", this.ttProyectosRow.empresa_ID);
                request.AddParameter("tipo_proyecto_id", this.ttProyectosRow.tipoproyecto_ID);
                request.AddParameter("proyecto_erp_id", this.ttProyectosRow.proyecto_ID);
                request.AddParameter("proyecto_erp_id2", this.ttProyectosRow.proyecto_ID2);
                request.AddParameter("sector", this.ttProyectosRow.sector);
                request.AddParameter("idioma_id", this.ttProyectosRow.idioma_ID);
                request.AddParameter("facturable", this.ttProyectosRow.facturable);
                request.AddParameter("habilitado", this.ttProyectosRow.habilitado);
                request.AddParameter("cliente_id", cliente.id);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El Cliente " + this.ttProyectosRow.cliente_ID.ToString() + " no existe en la nube.";
                return false;
            }
        }

        private bool Put(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            var proyecto = ttProyectos.Get(_url, _email, _pass, this.ttProyectosRow.proyecto_ID, this.ttProyectosRow.empresa_ID);
            if (proyecto != null)
            {
                var cliente = ttCliente.Get(_url, _email, _pass, this.ttProyectosRow.cliente_ID, this.ttProyectosRow.empresa_ID.ToString());
                if (cliente != null)
                {
                    var client = new RestClient(_url);
                    var request = new RestRequest("proyecto/" + proyecto.id.ToString(), Method.PUT);
                    request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                    request.AddParameter("name", this.ttProyectosRow.descripcion);
                    request.AddParameter("empresa_id", this.ttProyectosRow.empresa_ID);
                    request.AddParameter("tipo_proyecto_id", this.ttProyectosRow.tipoproyecto_ID);
                    request.AddParameter("proyecto_erp_id", this.ttProyectosRow.proyecto_ID);
                    request.AddParameter("proyecto_erp_id2", this.ttProyectosRow.proyecto_ID2);
                    request.AddParameter("sector", this.ttProyectosRow.sector);
                    request.AddParameter("idioma_id", this.ttProyectosRow.idioma_ID);
                    request.AddParameter("facturable", this.ttProyectosRow.facturable);
                    request.AddParameter("habilitado", this.ttProyectosRow.habilitado);
                    request.AddParameter("cliente_id", cliente.id);
                    IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        ErrorMessage = string.Empty;
                        return true;
                    }
                    else if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        ErrorMessage = "Internal Server Error.";
                        return false;
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        ErrorMessage = response.Content;
                        return false;
                    }
                    else
                    {
                        ErrorMessage = response.Data.rta;
                        return false;
                    }
                }
                else
                {
                    ErrorMessage = "El cliente " + this.ttProyectosRow.cliente_ID+ " no existe en la nube.";
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El proyecto " + this.ttProyectosRow.proyecto_ID + " no existe en la nube.";
                return false;
            }
        }

        private bool Put(string _url, string _token, out string ErrorMessage)
        {
            var proyecto = ttProyectos.Get(_url, _token, this.ttProyectosRow.proyecto_ID, this.ttProyectosRow.empresa_ID);
            if (proyecto != null)
            {
                var cliente = ttCliente.Get(_url, this.ttProyectosRow.cliente_ID, this.ttProyectosRow.empresa_ID.ToString(), _token);
                if (cliente != null)
                {
                    var client = new RestClient(_url);
                    var request = new RestRequest("proyecto/" + proyecto.id.ToString(), Method.PUT);
                    request.AddParameter("token", _token);
                    request.AddParameter("name", this.ttProyectosRow.descripcion);
                    request.AddParameter("empresa_id", this.ttProyectosRow.empresa_ID);
                    request.AddParameter("tipo_proyecto_id", this.ttProyectosRow.tipoproyecto_ID);
                    request.AddParameter("proyecto_erp_id", this.ttProyectosRow.proyecto_ID);
                    request.AddParameter("proyecto_erp_id2", this.ttProyectosRow.proyecto_ID2);
                    request.AddParameter("sector", this.ttProyectosRow.sector);
                    request.AddParameter("idioma_id", this.ttProyectosRow.idioma_ID);
                    request.AddParameter("facturable", this.ttProyectosRow.facturable);
                    request.AddParameter("habilitado", this.ttProyectosRow.habilitado);
                    request.AddParameter("cliente_id", cliente.id);
                    IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        ErrorMessage = string.Empty;
                        return true;
                    }
                    else if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        ErrorMessage = "Internal Server Error.";
                        return false;
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        ErrorMessage = response.Content;
                        return false;
                    }
                    else
                    {
                        ErrorMessage = response.Data.rta;
                        return false;
                    }
                }
                else
                {
                    ErrorMessage = "El cliente " + this.ttProyectosRow.cliente_ID + " no existe en la nube.";
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El proyecto " + this.ttProyectosRow.proyecto_ID + " no existe en la nube.";
                return false;
            }
        }

        private bool Delete(string _url, string _email, string _pass, 
            out string ErrorMessage)
        {
            var proyecto = ttProyectos.Get(_url, _email, _pass, this.ttProyectosRow.proyecto_ID);
            if (proyecto != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("proyecto/" + proyecto.id.ToString(), Method.DELETE);
                request.AddParameter("token", API.GetInstance(_email, _pass, _url).Token);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El proyecto " + this.ttProyectosRow.proyecto_ID + " no existe en la nube.";
                return false;
            }
        }

        private bool Delete(string _url, string _token, out string ErrorMessage)
        {
            var proyecto = ttProyectos.Get(_url, _token, this.ttProyectosRow.proyecto_ID,this.ttProyectosRow.empresa_ID);
            if (proyecto != null)
            {
                var client = new RestClient(_url);
                var request = new RestRequest("proyecto/" + proyecto.id.ToString(), Method.DELETE);
                request.AddParameter("token", _token);
                IRestResponse<APIResponse> response = client.Execute<APIResponse>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ErrorMessage = string.Empty;
                    return true;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    ErrorMessage = "Internal Server Error.";
                    return false;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = response.Content;
                    return false;
                }
                else
                {
                    ErrorMessage = response.Data.rta;
                    return false;
                }
            }
            else
            {
                ErrorMessage = "El proyecto " + this.ttProyectosRow.proyecto_ID + " no existe en la nube.";
                return false;
            }
        }
    }

    [Serializable()]
    public class ttProyectosRow
    {
        [XmlElement("empresa_ID")]
        public int empresa_ID;

        [XmlElement("tipoproyecto_ID")]
        public int tipoproyecto_ID;

        [XmlElement("proyecto_ID")]
        public int proyecto_ID;

        [XmlElement("proyecto_ID2")]
        public string proyecto_ID2;

        [XmlElement("habilitado")]
        public int habilitado;

        [XmlElement("descripcion")]
        public string descripcion;

        [XmlElement("sector")]
        public string sector;

        [XmlElement("idioma_ID")]
        public int idioma_ID;

        [XmlElement("facturable")]
        public string facturable;

        [XmlElement("actualizar")]
        public string actualizar;

        [XmlElement("cliente_ID")]
        public string cliente_ID;
    }
}
